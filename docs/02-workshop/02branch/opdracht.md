# Opdracht

Aangezien jullie met twee gelijktijdig wijzigingen gaan aanbrengen aan de repo is het geen goed idee om dat in dezelfde branch te doen.  
Je kan er beter voor kiezen om elk in een aparte, nieuwe branch te werken en achteraf deze wijzigingen van beiden samen te brengen.  
Zo wordt er eigenlijk in ook bij softwareontwikkelteams gewerkt. Voor elke nieuwe feature of update wordt een nieuwe branch gemaakt.

## Branch 

Maak elk lokaal een branch aan met een duidelijke naam, bijvoorbeeld `movie-[moviename]`.

Nadat daarin een leeg bestand werd aangemaakt in de map /movies met de juiste bestandsnaam

## Content genereren

Vul elk de content in jullie eigen branch aan zodat jullie beide films 'compleet' zijn. Doe dat volgens de regels van de (html)-kunst.
De focus van deze opdracht is uiteraard niet om de meest literaire reviews te produceren, maar we proberen wel enkel degelijke reviews toe te voegen aan onze databank. 
Leg de lat dus hoog genoeg, anders is de kans groot dat je inzending geweigerd wordt..

## Conflicten vermijden

Jullie werken beide aan een verschillende review in een aparte branch.

Hou de commits klein en duidelijk. 

Een aantal taken die je kan verdelen in commits:
- titel toevoegen
- review toevoegen
- index-pagina aanpassen
- afbeelding toevoegen in de assets-folder
- ...

## Merge de featurebranch met main

Merge elk de eigen branch terug naar de main branch. Je kan dat via een lokale merge doen, of de gitlab UI gebruiken ("=merge request").
Als je de merge lokaal doet, moet uiteraard ook nog een push gebeuren naar de Gitlab remote.
