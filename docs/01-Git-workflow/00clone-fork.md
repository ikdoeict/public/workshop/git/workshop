---
id: clone-fork
title: Clone vs Fork
sidebar_position: 0
---

In Git zijn het klonen en forken van een repository twee verschillende manieren om een kopie van een bestaande repository te maken. 
Deze originele, bestaande repository is dus meestal aanwezig op een webgebaseerd git platform zoals Gitlab, Github, Bitbucket ...

**Klonen** wordt gebruikt wanneer je als `bijdrager of medewerker` aan een repository wilt werken, terwijl **forken** wordt gebruikt wanneer je als `onafhankelijke ontwikkelaar` aan een repository wilt werken. Je wil m.a.w. een eigen toevoeging of verbetering voorzien aan een repo waarvan jij niet de (mede-)eigenaar bent.

Hieronder staat wanneer je elk van deze methoden kan gebruiken:

## Klonen van een repository:

Wanneer je een repository kloont, maak je een kopie van de repository op je lokale machine. Klonen wordt typisch gebruikt wanneer je als bijdrager of medewerker van een project aan een repository wilt werken.  
Zoals eerder gezien kan je een repository klonen met de volgende opdracht:

```bash
git clone <repository URL>

```

Nadat je de repository hebt gekloond, kun je wijzigingen aan de code aanbrengen, nieuwe (feature-) branches maken en je wijzigingen terug naar de repository pushen.

## Forken van een repository:

Wanneer je een repository forkt, maak je eigenlijk een kopie van de repository in je eigen GitHub/Gitlab-account.  
Forken wordt typisch gebruikt wanneer je als onafhankelijke ontwikkelaar aan een repository (die niet in je beheer zit) wilt werken, of wanneer je wijzigingen aan dergelijke repository wilt bijdragen zonder medewerker of bijdrager te zijn. 
Het is een nuttige techniek om bijvoorbeeld bij bestaande open-source projecten bij te dragen aan de verbetering van het product zonder rechtstreekse invloed te hebben op de oorspronkelijke repository.  
Je kan dan, zonder enig gevaar, je eigen fork repository bewerken.

Als je denkt een geslaagde bewerking of verbetering ontwikkelt te hebben, dan kan je deze voordragen aan de originele repository met een merge/pull-request.

Typisch kan je een repository forken via de "Fork" knop in de rechterbovenhoek van de repository pagina op GitLab/GitHub.
Nadat je de repository hebt geforkt, kun je de geforkte repository naar je lokale machine klonen en wijzigingen aanbrengen.

Via een merge/pull-request kan je jouw wijzigingen voorstellen om opgenomen te worden in de oorspronkelijke repository.

<p align="center">
    <img src="/img/git/git_clone_vs_fork.png" alt="drawing" width="600"/>
</p>