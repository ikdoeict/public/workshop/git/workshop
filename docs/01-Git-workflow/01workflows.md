---
id: git-workflows
title: Git Workflows
sidebar_position: 1
---

## Git Workflows - Git Strategies

Na vorige hoofdstukken is branching en merging ondertussen geen geheim meer voor jou.
Maar éénmaal je in grote software development projecten terecht komt zal er met meerdere mensen samengewerkt moeten worden.
Wellicht zal er dan ook gelijktijdig aan verschillende features gewerkt worden en wenst men ten alle tijd bepaalde stabiele releases van het product ter beschikking hebben.
Veel softwareontwikkelingsbedrijven hanteren een Agile aanpak om hun product te ontwikkelen en willen daarnaast ook structureel de code aan testen onderwerpen.

Bovenstaande randvoorwaarden zorgen er voor dat je best een bepaalde, goed afgesproken, `git branching strategie` hanteert in je ontwikkelteam.

:::info
Welke Git branching strategie of Git workflow je gebruikt hangt sterk af van de grootte van het project, de grootte van het team, interne afspraken, etc...  
Meestal is er zelfs geen uniforme strategie maar zal elk bedrijf of ontwikkelteam een strategie hanteren die voor hen het best werkt.
:::

Hieronder vind je alvast een beperkt overzicht van enkele typische workflows.

### Basic Git Workflow

Dit concept is zeer eenvoudig: er is enkel een centrale repository. Elke ontwikkelaar kloont de repository, werkt lokaal aan de code, commit de wijzigingen en pusht deze naar de centrale repository zodat andere ontwikkelaars het kunnen gebruiken.

<p align="center">
    <img src="/img/git/Basic-git-workflow-2.png" alt="drawing" width="600"/>
</p>

Deze Git workflow wordt over het algemeen niet aanbevolen, tenzij je werkt aan een kleinschalig project en snel aan de slag moet.

Met slechts één branch is het proces eenvoudig, waardoor het gemakkelijk is om met Git te beginnen.  
Er zijn echter enkele nadelen bij het gebruik van deze workflow:

* Het samenwerken aan code kan leiden tot talloze conflicten aangezien iedereen in dezelfde (main) branch werkt. Iedereen overschrijft dus telkens een andere zijn code.
* De kans op het vrijgeven van buggy software naar productie is hoger. Er is geen tussenliggende branch waar uitgebreide testen kunnen uitgevoerd worden voor iets in deployment of release gaat.
* ...

### Git Flow (Feature Branches)

Dit is een Git strategie die tot voor kort heel veel toegepast werd. Het werd in 2010 voorgesteld dor de Nederlander Vincent Driessens.  
Een interssante en gedetailleerde uitleg kan je dan ook vinden in een [blogpost](http://datasift.github.io/gitflow/IntroducingGitFlow.html) van deze bedenker.

Het is gebaseerd op 2 hoofd branches die permanent blijven bestaan.

* `master` — Deze branch bevat de productiecode. Alle ontwikkelingscode, afkomstig van development, wordt gemerged op deze master branch. Deze branch ontvangt enkel merges van de "development-", "release-" en "hotfix-" branches.
* `develop` — Deze branch bevat pre-productiecode. Verschillende ontwikkelde features komen, wanneer voltooid, in deze branch terecht. Deze branch wordt ook gebruikt als "staging environment".

---

Bovenstaande branches worden dan verder aangevuld tijdens het ontwikkelproces met extra branches die kortstondig of beperkt blijven bestaan. Er zijn uiteraard nog veel andere varianten te vinden.

* `feature-*` — Feature branches worden gebruikt om nieuwe functies te ontwikkelen voor aankomende releases. Deze branch is afgeleid van develop en moet nadien terug samengevoegd worden met develop.
* `hotfix-*` — Hotfix branch is nodig om direct actie te ondernemen bij onvoorziene problemen (bugs) in de master. Deze branch is dus afgeleid van de master en moet, na het oplossen van de bug, terug gemerged worden met zowel master als develop.
* `release-*` — Release branches ondersteunen de voorbereiding van een nieuwe productierelease. Deze wordt "afgetakt" van de development-branch nadat deze enkele belangrijke nieuwe features verzameld heeft. In deze release-branch kunnen nog kleine bugs opgelost  en metadata toegevoegd worden. Uiteraard zal er hier nog eens uitgebreid getest worden. Deze branch krijgt typisch de naam "release" aangevuld met de versienummer van deze release (vb 1.0). Eenmaal volledig getest en klaar voor deployment wordt deze branch gemerged naar zowel de main als de development branch.

<p align="center">
    <img src="/img/git/git-model@2x.png" alt="drawing" width="400"/>
</p>

Niettegenstaande Git Flow een goed uitgedokterde strategie is om in grotere teams samen te werken aan softwareprojecten met verschillende release-versies, zijn er toch enkele nadelen.

* De Git-geschiedenis kan onleesbaar worden tenzij elke feature-branch een "rebase" krijgt voordat deze op de development-branch wordt samengevoegd. Zelfs als dit gebeurt, is het echter geen garantie dat de geschiedenis mooi overzichtelijk blijft.
* Het samenvoegen van elke feature in de develop branch kan het releaseproces vertragen. Het team besluit alleen een release branch aan te maken wanneer een set van features voltooid is, en zodra de releasekandidaat gereed is, kan de nieuwe versie naar productie worden gepromoot. Dit is helemaal niet handig in het kader van continuous integration and continuous delivery (CI/CD).

In onderstaande video wordt deze strategie nog eens mooi toegelicht:

<p align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/hG_P6IRAjNQ?start=132" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</p>

### GitHub Flow

Deze Git strategie is heel eenvoudig. GitHub Flow bestaat uit slechts één branch (meestal genaamd "master" of "main") die dient als de hoofdlijn voor alle ontwikkelingswerkzaamheden. Feature branches worden aangemaakt vanaf de hoofdbranch en samengevoegd zodra de feature afgerond is. Er zijn dus geen tussenliggende release branches meer. De "main" branch is dus altijd "productieklaar" voor de klant.  
Heel belangrijk bij deze strategie is dat er in de feature-branches zelf heel uitgebreide testen en controles zullen gebeuren (CI/CD pipelines). Elke feature-branch zal wellicht ook zijn eigen test- en staging environment toegewezen krijgen om alle effecten goed te kunnen testen.  
Commits die "gereleased" mogen worden naar de klant (main-branch) krijgen, voorafgaand aan de Merge Request, meestal een (release-)**"tag"** mee om dat aan te geven.

Deze strategie is ideaal waar korte release cycles gevraagd worden en waar **"continuous integration & continuous delivery"** belangrijk zijn.

<p align="center">
    <img src="/img/git/gitHub-Flow.png" alt="drawing" width="800"/>
</p>

In onderstaande video wordt deze strategie nog eens mooi toegelicht:

<p align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/hG_P6IRAjNQ?start=278" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</p>

Ook hier zijn enkele nadelen:

* Feature branches blijven meestal langer bestaan (long-lived)
* Vanwege onmiddellijke overgang naar "production/deployment" kunnen fouten makkelijk door de mazen van het net glippen indien onvoldoende testen uitgevoerd werden.
* Bij grotere teams snel gevaar voor merge conflicts
* Meerdere release-versies bijhouden is moeilijk.

### Gitlab Flow

Deze aanpak situeert zich wat tussen Git Flow en GitHub Flow. In vergelijking met de eenvoudige GitHub Flow worden er twee extra branches voorzien. De main-branch is niet meer onmiddellijk de "production" versie.
De twee extra branches dienen voor `Staging` en `Production`. Er worden voor deze branches aparte **Environments** voorzien waarin de code gedeployed zal worden. De main-branch dient dan vooral als `test-environment`.

Net zoals in de vorige twee strategieën worden nieuwe functies ontwikkeld in een aparte feature branches.
Als de feature "klaar" is kan deze met een merge request samengevoegd worden met de master branch daar onderworpen worden aan uitgebreide testen. 
Na verloop van tijd, wanneer er meerdere functies zijn samengevoegd met de master branch, wordt de master branch samengevoegd met de preproductie-omgeving (staging). Dit stelt het team in staat om opnieuw te testen en te valideren of alle functies correct werken in een omgeving die vergelijkbaar is met de productieomgeving.
Na het testen en valideren in preprod, wordt de preprod-omgeving samengevoegd met de productieomgeving ("prod"). Dit zorgt ervoor dat alle nieuwe functies en wijzigingen in de productieomgeving beschikbaar zijn voor eindgebruikers.

<p align="center">
    <img src="/img/git/gitLab-Flow.png" alt="drawing" width="600"/>
</p>

<p align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/ZJuUz5jWb44?start=81" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</p>
