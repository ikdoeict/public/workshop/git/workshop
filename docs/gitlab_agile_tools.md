---
id: gitlab-agile
title: Gitlab Agile Tools
sidebar_position: 5
---

Git wordt op vele manieren ingezet maar bij uitstek wordt het gebruikt om in grotere ontwikkelteams gestructureerd code te schrijven en te onderhouden.
Je leerde eerder al de technieken zoals "branching", 'merging" en "Git branching strategies" die dit proces ondersteunen.

In de praktijk zullen heel wat softwarehuizen en hun ontwikkelteams een "`Agile`" aanpak hanteren om die software te ontwikkelen.
"Agile" werken is vooral een filosofie, een manier van denken en werken. Er zijn verschillende methodologieën die een "Agile" aanpak mogelijk maken.
De bekendste daarvan zijn "`Scrum`", "`Kanban`", "`Extreme Programming`", "`Feature Driven Development`".

Omdat dergelijke "Agile" aanpak heel nauw aansluit bij de manier waarop je software met je team ontwikkelt en onderhoudt, zijn er in de typische online Git Management Platformen zoals GitHub en Gitlab specifieke tools hiervoor geïntegreerd.
Dus naast de basisfunctionaliteit om versie beheer mogelijk te maken kan je, in deze Git platformen, als ontwikkelteam je Agile aanpak mee opnemen.

Onderstaande uitleg zal gebruik maken van Gitlab om deze features uit te leggen. In andere platformen zoals GitHub zijn deze zeer gelijkaardig.
Ook de ondersteunende [website van Gitlab](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/) vind je een gelijklopende uitleg.

:::tip
Het is belangrijk aan te geven dat er geen uniforme manier is om deze Agile Tools te gebruiken. Veel hangt af van de gewoontes of procedures die in je team toegepast worden. Je kan deze tools dus op heel veel verschillende manieren inzetten. Uiteraard is het wel héél belangrijk om goed af te spreken welke workflow je hier zal aanhouden.
:::

## Terminologie - Gitlab Agile Tools

* `Issues`: Starten met een issue is een veelvoorkomende praktijk in Agile ontwikkeling omdat het helpt om een specifieke businesswaarde voor de eindgebruiker te definiëren. Binnen "Agile" komt dit overeen met een `User Story`.  
In GitLab kunnen issues aangemaakt worden om vereisten (cfr de user stories), bugs of verbeteringen vast te leggen. Issues kunnen worden toegewezen aan teamleden, gelabeld en geprioriteerd. Ze kunnen ook worden gekoppeld aan andere issues, merge requests of epics. Een beknopt voorbeeld van een Issue: "Create design login form".

* `Tasks`: Dit zijn kleinere eenheden van werk die nodig zijn om een gebruikersverhaal (User Story) te voltooien. In GitLab kunt u een takenlijst maken binnen de beschrijving van een issue om zo een gebruikersverhaal op te splitsen in afzonderlijke taken. Taken kunnen worden afgevinkt wanneer ze zijn voltooid, waardoor er zichtbaarheid is in de voortgang.

* `Issue boards`: De issue boards van GitLab bieden een Kanban-stijlweergave van issues, waarmee teams de voortgang van hun project kunnen bijhouden en issues door verschillende ontwikkelingsstadia kunnen verplaatsen. Issues kunnen worden gesleept en neergezet tussen kolommen, waardoor teams een visuele weergave van hun workflow krijgen. Dergelijke "Issue boards" worden interessant als je zelf toepasselijke kolommen bijmaakt zoals "Ready for Testing", "Design",...  
Een makkelijke manier van werken is bv de verschillende kolommen van het Issue Board te voorzien op basis van de "labels" (zie verder). Op die manier zullen "Issues" automatisch verplaatst worden op het bord als er andere labels toegekend worden.

* `Issue Weights`: In GitLab kunnen gewichten toegewezen worden aan issues om hun geschatte werkinspanning aan te geven. Dit helpt teams om hun werk te plannen. Het bepalen van deze gewichten vraagt wat ervaring. In een Scrum-setting wordt dit dikwijls via "Scrum-poker" vastgelegd.

* `Labels`: Labels zijn een manier om issues en merge requests te categoriseren in GitLab. Ze kunnen worden gebruikt om de lijsten van issues te filteren op een enkel label of meerdere labels, waardoor een manier ontstaat om snel relevante issues te vinden. Labels kunnen ook worden gebruikt om de voortgang bij te houden of issues te identificeren die aandacht nodig hebben.

* `Epics`: Epics zijn groepen van gerelateerde issues die een gemeenschappelijk thema of doel delen (een verzameling van gerelateerde User Stories). In GitLab kunnen epics gemaakt worden om **de voortgang over meerdere projecten** bij te houden. Epics bieden een overzicht op een hoger niveau (Group/Subgroup in Gitlab) van de voortgang. Een Epic kan je dus niet aanmaken in een project/repo maar wel in een bovenliggende (sub-)group. Een voorbeeld van een Epic: "Implement User Authentication in website". (Epics zijn in principe niet tijdsgebonden, al kan er daar ook wel een start- en einddatum aan vastgehangen worden of kunnen deze gekoppeld worden met Milestones)

* `Milestones - Iterations`: Dit zijn twee termen die tijdsgerelateerd zijn. Beiden geven een indicatie over de voortgang van het project in de tijd. De eerder gemaakte "Issues" kunnen op die manier toegewezen worden aan een "Milestone" en/of een "Iteration". Beide termen kunnen gelijktijdig gebruikt worden of je kan je beperken tot één van deze twee afhankelijk van de situatie.
  * **Milestones** geven een overzicht in een bepaalde periode of tijdsframe. Milestones kunnen een langer tijdsbestek bestrijken en zijn meestal gerelateerd met Epics.  Op die manier kan je vlot zien hoeveel Issues afgewerkt zijn in een bepaalde tijd waarbinnen je een bepaald doel (Epic) wil bereiken.
  * **Iterations** zijn een typisch Agile gegeven. Een iteratie komt overeen met een **sprint** en bepaalt een wederkerende tijdscyclus die in een development team gehanteerd wordt. Typisch wordt in een bijeenkomst (sprint planning) vooraf vastgelegd welke issues behandeld zullen worden in de komende sprint. Iterations of Sprints moeten vastgelegd worden op het hoogste niveau (Group of Sub-Group).

* `Roadmaps`: De roadmaps van GitLab bieden een manier om de start- en einddatums van Epics en Milestones in een tijdslijn te visualiseren. De roadmap-pagina toont alle epics onder een groep of de subgroepen ervan, waardoor teams kunnen zien hoe verschillende projecten vorderen.

* `Burndown Chart`: Burndown charts zijn een manier om de voortgang in real-time bij te houden. In GitLab tonen burndown charts het werk dat is gepland in een tijdsframe terwijl het wordt voltooid. Dit stelt teams in staat om hun plannen aan te passen en middelen toe te wijzen indien nodig. Burndown Charts geven aan **hoeveel "werk"** (Issues rekening houdend met hun bijhorende "weights") **nog moet uitgevoerd worden in dit tijdsbestek**. Idealiter volgt deze curve de dalende rechte (Guideline).  
Dergelijke charts tonen het tijdsframe op basis van `Iterations` (sprints) of `Milestones`.

* `Burnup Chart`: Dit is gelijkaardig aan de Burndown Chart maar geeft aan h**oeveel werk reeds afgerond werd tijdens een bepaald tijdsframe** (Iteration of Milestone).

## Een voorbeeld - Use Case

Het ontwikkelteam bij "MySoftwareCompany" heeft de taak gekregen om een nieuwe e-commerce website voor een klant te bouwen. Het team begint door deze opdracht op te delen in kleinere gebruikersverhalen (User Stories) met behulp van de functie `Issues` in GitLab. Bijvoorbeeld, een issue zou kunnen zijn *"Als klant wil ik in staat zijn om items aan mijn winkelwagen toe te voegen."*. Eventueel voegen ze aan deze issue nog enkele kleinere `Tasks` toe.

Terwijl het team begint te werken aan deze issues, realiseren ze zich dat sommige ervan gerelateerd zijn en bij elkaar gegroepeerd moeten worden. Ze gebruiken de functie `Epics` in GitLab om een nieuwe epic genaamd *"Shopping Cart"* te maken en alle relevante issues eraan toe te voegen.

Om de voortgang bij te houden, maakt het team `Milestones` aan in GitLab. Bijvoorbeeld, ze kunnen een mijlpaal genaamd *"Minimum viable product"* aanmaken en een deadline instellen voor over drie maanden. Vervolgens wijzen ze elk issue toe aan de juiste mijlpaal.

Terwijl het team werkt aan de issues, gebruiken ze de functie `Issue weights` in GitLab om de inspanning te schatten die nodig is voor elk issue. Bijvoorbeeld, ze kunnen een gewicht van 5 toekennen aan het *"Add to Cart"* issue omdat het relatief eenvoudig is, maar een gewicht van 13 toekennen aan het *"Checkout process"* issue omdat het complexer is.

Met behulp van de functie `Burndown chart` in GitLab kan het team hun voortgang in realtime bijhouden en hun werk aanpassen om aan hun doelen te voldoen. Als ze issues voltooien, laat de Burndown chart zien hoe het resterende werk afneemt na verloop van tijd. Als ze achterlopen op schema, kunnen ze de chart gebruiken om de issues te identificeren die langer duren dan verwacht en hun prioriteiten overeenkomstig aanpassen.

